import scala.util.control._

class MyList {
  var FirstNode:Node = null 
  var LastNode:Node = null 
  
  var size=0;
  
  def createList:MyList = new  MyList()
  
  def insert(Account:Account){
    var newNode=new Node(Account)
    
    if (size == 0){
      FirstNode =  newNode
      LastNode = FirstNode
    }
    else{      
      var currNode=FirstNode
      var prevNode=FirstNode
      var condition = true
      
      while(condition){        
        
        if ((newNode.getAccount.value<currNode.getAccount.value)||(currNode.NextNode==null))
            condition = false
        else {
          prevNode=currNode
          currNode=currNode.NextNode
          }        
      }
      
      if((currNode==FirstNode)&&(newNode.getAccount.value<currNode.getAccount.value)){
        newNode.NextNode=currNode
        FirstNode=newNode    
      }      
      else
        if((currNode.NextNode==null)&&(newNode.getAccount.value>currNode.getAccount.value)){          
          LastNode.setNode(newNode)
          LastNode=newNode
        }else{
          newNode.NextNode=currNode
          prevNode.NextNode=newNode          
        }
      
    }
    size=size+1
  }
  /*
  def insertArray(A:Array[Int]){    
    var i=0
    for( i <- 0 to A.length-1){
        insert(A(i))
      }       
  }*/
  
  
  def isEmpity():Int={
    if (size==0) 1
    else 0
  }
   
  def printList(){
    var i =0;    
    var currNode=FirstNode
    for( i <- 1 to size){
      println( "Value of Node: " + currNode.getAccount.value );
      currNode=currNode.NextNode
      }   
    
  }
  def printListRecursive(currNode: Node){
      println( "Value of Node: " + currNode.getAccount.value );
      if (currNode.NextNode!=null)
        printListRecursive(currNode.NextNode) 
  }
  
  def printListReverse(currNode: Node){
      if (currNode.NextNode!=null){
        printListReverse(currNode.NextNode)    
      }  
      println( "Value of Node: " + currNode.getAccount.value ) 

  }
  def find(value: Int):Node={
    var i =0;    
    var currNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){
        if(value==currNode.getAccount.value){
          //println( "Find Node "+value+" in position " + i );
          
          loop.break
        }else currNode=currNode.NextNode
      } 
      
      if(i!=size) println( "Account Not Find");
    }
    currNode
  }
  
  def delete(value: Int){
    
    var i =0;    
    var currNode=FirstNode    
    var prevNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){        
        if(value==currNode.getAccount.value){
          if(i==1){
            FirstNode=currNode.NextNode
          }else if(i==size){
            LastNode=prevNode
            prevNode.NextNode=null;
          }else{
            prevNode.NextNode=currNode.NextNode            
          }
          
          size=size-1
          println( "Conta de número: "+ value+" deletada");
          loop.break
        }else {
          prevNode=currNode
          currNode=currNode.NextNode
          }        
      } 
      
      if(i!=size) println( "Not Find Node:Can't delete");
    }
    
  }
  
  def deleteRecursive(value: Int,currNode: Node,prevNode: Node){
    if(currNode!=null){
      if(currNode.getAccount.value==value){
        if(prevNode==currNode){
          FirstNode=currNode.NextNode
        }else if(currNode.NextNode==null){
            LastNode=prevNode
            prevNode.NextNode=null
          }else{
            prevNode.NextNode=currNode.NextNode            
            }
        size=size-1
        println( "Deleted node (recursive)"+ value);
      }else deleteRecursive(value,currNode.NextNode,currNode)
    }  
  }
  
  def free(){
    recursiveFree(FirstNode)
    size=0;
    FirstNode=null;
    LastNode=null;
  }
  
  def recursiveFree(currNode: Node){  
      if (currNode!=null){
        recursiveFree(currNode.NextNode)
        currNode.NextNode=null;
      }             
  }
  
}