
 object Main {
    def main(args: Array[String]): Unit = {
      //chamada das funções básicas e povoamento
      //criando uma lista vazia
       
      val q= new MyList().createList
      //inserindo elementos(desordenados, mas que serão ordenados pelo insert sort) o primeiro número é referente ao número da conta
     
      println("Inserindo valores")
      q.insert(new BankAccount(1,100))      
      q.insert(new BankAccount(4,1000))           
      q.insert(new BankAccount(14,1000)) 
      
      q.insert(new SavingsAccount(6,300,0.2))      
      q.insert(new SavingsAccount(3,3000,0.11))          
      q.insert(new SavingsAccount(10,2300,0.11))
      
      q.insert(new LoyaltyAccount(5,300,100,0.13))      
      q.insert(new LoyaltyAccount(2,3000,100,0.1))      
      q.insert(new LoyaltyAccount(7,300,100,0.13)) 
      
      //printando elementos
      println("Printando ")
      q.printList()
      
      //deletando elementos
      
      println("Deletando um valor")
      q.delete(3)
 
      //printando de forma recursiva
      println("Printando recurssivamente")
      q.printListRecursive(q.FirstNode)
      
      println("Deletando recurssivamente")
      //deletando de forma recursiva
      q.deleteRecursive(4, q.FirstNode, q.FirstNode)      

      println("Printando ao contrário")
      //printando ao contrário
      q.printListReverse(q.FirstNode)      
      
      //libera a lista      
      println("Esvaziando a lista")
      q.free()
      
      //checka se esta vazia      
      println("Checando se a lista esta vazia")
      println("Is empity:"+q.isEmpity())
      
      
      val account= new TADAccount()
      
      //1. Inserir uma conta bancária;numero da conta /saldo
      account.insertBankAccount(4401,1000) 
      account.insertBankAccount(4402,1000) 
      account.insertBankAccount(4403,1000)       
      
      //2. Inserir uma conta poupança;numero da conta / saldo/ taxa de juros
      
      account.insertSavingsAccount(5501, 1000, 0.1)
      account.insertSavingsAccount(5503, 1000, 0.2)
      account.insertSavingsAccount(5502, 1000, 0.3)      
      
      //3. Inserir uma conta fidelidade;numero da conta / saldo, bonus inicial , taxa do bonus
      
      account.insertLoyaltyAccount(6603, 1000, 0, 0.1)
      account.insertLoyaltyAccount(6601, 1000, 0, 0.1)
      account.insertLoyaltyAccount(6602, 1000, 0, 0.1)

      
      //4. Realizar crédito em uma determinada conta;
      account.credit(6603, 100)
      
      //5. Realizar débito em uma determinada conta;
      account.debit(4403, 100)
      
      //6. Consultar o saldo de uma conta;
      account.consultBalance(6603)
      
      //7. Consultar o bônus de uma conta fidelidade;
      account.consultBonus(6603)
      
      //8. Realizar uma transferência entre duas contas;
      account.transfer(5503, 4403, 100)
      
      //9. Render juros de uma conta poupança;
      account.applyBonus(6603)
      
      //10. Render bônus de uma conta fidelidade;
      account.applyInterest(5501)

      //11. Remover uma conta;
      account.deleteAccount(5502)  
      //12. Imprimir número e saldo de todas as contas cadastradas;
      account.printBalance()
      
      
      
    }
  }