

trait Account {
    var balance:Double=0    
    val value:Int

    def credit(value:Double)
    
    def debit(value:Double){
      balance=balance-value
    }
    
    def getBalance():Double={
      balance
    }

}