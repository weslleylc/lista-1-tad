

class SavingsAccount(Value:Int,Balance:Double,TX:Double) extends Account{
  val value=Value
  balance=Balance
  var tx:Double=TX
  
  def applyInterest{
    balance=balance+balance*tx;
  }
  
  def credit(value:Double){
     balance=balance+value
  }
}