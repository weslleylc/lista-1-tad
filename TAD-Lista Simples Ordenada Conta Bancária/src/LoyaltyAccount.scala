

class LoyaltyAccount(Value:Int,Balance:Double,Bonus:Double,TX:Double) extends Account {
  val value=Value
  balance=Balance
  var bonus:Double=Bonus
  var tx:Double=TX
  
  def applyBonus{
    balance=balance+bonus
    bonus=0
  }
  
  def getBonus():Double={
      bonus
    } 
   
  def credit(value:Double){
      balance=balance+value
      bonus=bonus+TX*value
    }  
}