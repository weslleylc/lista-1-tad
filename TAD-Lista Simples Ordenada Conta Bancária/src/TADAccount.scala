

class TADAccount {
  
  val mylist = new MyList().createList
  
  def insertBankAccount(Value:Int,Balance:Double){
    mylist.insert(new BankAccount(Value,Balance)) 
  }
  def insertLoyaltyAccount(Value:Int,Balance:Double,Bonus:Double,TX:Double){
    mylist.insert(new LoyaltyAccount(Value,Balance,Bonus,TX))
  }
  def insertSavingsAccount(Value:Int,Balance:Double,TX:Double){
    mylist.insert(new SavingsAccount(Value,Balance,TX)) 
  }
  def credit(Value:Int,cred:Double){
    var t = mylist.find(Value)
    if(t!=null)
      t.getAccount().credit(cred)
  }
  def debit(Value:Int,deb:Double){ 
    var t = mylist.find(Value)
    if(t!=null)
      t.getAccount().debit(deb)
  }
  def consultBalance(Value:Int){
    var t = mylist.find(Value)
    if(t!=null)
      println("Conta número: "+Value+" Valor total da conta:" + t.getAccount().getBalance())
  }
  def consultBonus(Value:Int){
    var t = mylist.find(Value)
    if(t!=null)
      println("Conta número: "+Value+" Valor total do Bônus:" + t.getAccount().asInstanceOf[LoyaltyAccount].getBonus) 
  }
  def transfer(Value1:Int,Value2:Int,Value3:Double){
    var t1 = mylist.find(Value1)
    var t2 = mylist.find(Value2)
    
    if((t1!=null)&&(t2!=null))
    {
      debit(Value1,Value3)
      credit(Value2,Value3)
    }
  }
  def applyInterest(Value:Int){
    var t = mylist.find(Value)
    if(t!=null)
      t.getAccount().asInstanceOf[SavingsAccount].applyInterest
  }
  def applyBonus(Value:Int){
    var t = mylist.find(Value)
    if(t!=null)
      t.getAccount().asInstanceOf[LoyaltyAccount].applyBonus
  }
  def deleteAccount(Value:Int){
    mylist.delete(Value)
  }
  def printBalance(){    
    var i =0;    
    var currNode=mylist.FirstNode
    for( i <- 1 to mylist.size){
    println( "Conta: " + currNode.getAccount.value +" Valor do saldo: "+currNode.getAccount.getBalance());
    currNode=currNode.NextNode
    } 
  }
  
}