

object MainAccount {
  def main(args: Array[String]): Unit = {
      
   //chamada das funções básicas e povoamento
      //criando uma lista vazia
      val q= new MyList().createList
      //inserindo elementos(desordenados, mas que serão ordenados pelo insert sort) o primeiro número é referente ao número da conta
      
      println("Inserindo valores")
      q.insert(new BankAccount(1,100))      
      q.insert(new BankAccount(4,1000))           
      q.insert(new BankAccount(14,1000)) 
      
      q.insert(new SavingsAccount(6,300,0.2))      
      q.insert(new SavingsAccount(3,3000,0.11))          
      q.insert(new SavingsAccount(10,2300,0.11))
      
      q.insert(new LoyaltyAccount(5,300,100,0.13))      
      q.insert(new LoyaltyAccount(2,3000,100,0.1))      
      q.insert(new LoyaltyAccount(7,300,100,0.13)) 
      
      //printando elementos
      println("Printando ")
      q.printList()
      
      //deletando elementos
      
      println("Deletando um valor")
      q.delete(3)
 
      //printando de forma recursiva
      println("Printando recurssivamente")
      q.printListRecursive(q.FirstNode)
      
      println("Deletando recurssivamente")
      //deletando de forma recursiva
      q.deleteRecursive(4, q.FirstNode, q.FirstNode)      

      println("Printando ao contrário")
      //printando ao contrário
      q.printListReverse(q.FirstNode)      
      
      //libera a lista      
      println("Esvaziando a lista")
      q.free()
      
      //checka se esta vazia      
      println("Checando se a lista esta vazia")
      println("Is empity:"+q.isEmpity())
      
    }
}