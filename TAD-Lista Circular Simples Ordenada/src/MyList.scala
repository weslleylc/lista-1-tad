

import scala.util.control._

class MyList {
  var FirstNode:Node = null 
  var LastNode:Node = null 
  
  var size=0;
  
  def createList:MyList = new  MyList()
  
  def insert(Value:Int){
    var newNode=new Node(Value)
    
    if (size == 0){      
      newNode.NextNode=newNode
      FirstNode =  newNode
      LastNode = FirstNode
    }
    else{      
      var currNode=FirstNode
      var prevNode=FirstNode
      var condition = true
      var i=1;
      while(condition){        
        if ((newNode.value<currNode.value)||(i>=size))
            condition = false
        else {
          prevNode=currNode
          currNode=currNode.NextNode
          }   
        i=i+1
      }
      
      if((newNode.value<currNode.value)&&(currNode==FirstNode)){
        newNode.NextNode=currNode
        FirstNode=newNode    
        LastNode.NextNode=newNode
      }      
      else
        if((i>=size)&&(newNode.value>currNode.value)){        
          newNode.NextNode=FirstNode
          LastNode.setNode(newNode)
          LastNode=newNode          
        }else{
          newNode.NextNode=currNode
          prevNode.NextNode=newNode          
        }
      
    }
    size=size+1
  }
  
   def insertArray(A:Array[Int]){    
    var i=0
    for( i <- 0 to A.length-1){
        insert(A(i))
      }       
  }
  
  
  def isEmpity():Int={
    if (size==0) 1
    else 0
  }
   
  def printList(){
    var i =0;    
    var currNode=FirstNode
    for( i <- 1 to size){
      println( "Value of Node: " + currNode.value );
      currNode=currNode.NextNode
      }   
    
  }
  def printListRecursive(currNode: Node,i: Int){
      println( "Value of Node: " + currNode.value );
      if (i>1)
        printListRecursive(currNode.NextNode,i-1) 
  }
  
  def printListReverse(currNode: Node,i: Int){
      if (i>1){
        printListReverse(currNode.NextNode,i-1)    
      }  
      println( "Value of Node: " + currNode.value )      
  }
  
  def find(value: Int):Node={
    var i =0;    
    var currNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){
        if(value==currNode.value){
          println( "Find Node "+value+" in position " + i );
          
          loop.break
        }else currNode=currNode.NextNode
      } 
      
      if(i!=size) println( "Not Find Node");
    }
    currNode
  }
  
  def delete(value: Int){
    
    var i =0;    
    var currNode=FirstNode    
    var prevNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){        
        if(value==currNode.value){
          if(i==1){
            FirstNode=currNode.NextNode
          }else if(i==size){
            LastNode=prevNode
            prevNode.NextNode=FirstNode;
          }else{
            prevNode.NextNode=currNode.NextNode            
          }
          
          size=size-1
          println( "Deleted node "+ value);
          loop.break
        }else {
          prevNode=currNode
          currNode=currNode.NextNode
          }        
      } 
      
      if(i!=size) println( "Not Find Node:Can't delete");
    }
    
  }
  
  def deleteRecursive(value: Int,i: Int,currNode: Node,prevNode: Node){
    if(i>=1){
      if(currNode.value==value){
        if(prevNode==currNode){
          FirstNode=currNode.NextNode
        }else if(i==1){
            LastNode=prevNode
            prevNode.NextNode=FirstNode
          }else{
            prevNode.NextNode=currNode.NextNode            
            }
        size=size-1
        println( "Deleted node (recursive)"+ value);
      }else deleteRecursive(value,i-1,currNode.NextNode,currNode)
    }  
  }
  
  def free(){
    recursiveFree(FirstNode,size)
    size=0;
    FirstNode=null;
    LastNode=null;
  }
  
  def recursiveFree(currNode: Node,i: Int){  
      if (i>=1){
        recursiveFree(currNode.NextNode,i-1)
        currNode.NextNode=null;
      }             
  }
  
}
