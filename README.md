# README #

Implementações referentes a primeira lista

Cada questão da lista foi feita em um projeto separado, para compilar basta importar usando a IDE eclipse, ou usar algum compilador de sua preferência (o scalac no ubunto por exemplo)

Cada um dos repositórios contem um objeto "main" que chama todas as funções descritas no problema.

Além disso, alguns testes são feitos dependendo do tipo de estrutura escolhida, como mostrar o valor do primeiro nó tendo como base o ultimo nó (visto na lista circular  Ultimo nó -> Primeiro Nó)