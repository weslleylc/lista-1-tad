
class Node (VALUE:Int){
  def value:Int=VALUE
  var NextNode:Node = null  
  var PrevNode:Node = null
  
  def setNode(NewNode: Node) {
    NextNode = NewNode    
  }
  
  def setPrevNode(NewNode: Node) {
    PrevNode = NewNode    
  }
  
  def setNodes(nestNode: Node,prevNode: Node){
    NextNode = nestNode
    PrevNode = prevNode
  }
}