


 object Main {
    def main(args: Array[String]): Unit = {
      
      
      //1.Criar uma lista vazia;
      val mylist= new MyList().createList

      //2. Inserir elemento no início;
      mylist.insertArray(Array(1,3,4,2,5,6,7,4,9,10))
      
      //3. Imprimir os valores armazenados na lista;
      mylist.printList()
      
      //4. Imprimir os valores armazenados na lista usando recursão;
      mylist.printListRecursive(mylist.FirstNode)
      
      //5. Imprimir os valores armazenados na lista em ordem reversa (da cauda para a cabeça da lista);
      mylist.printListReverse(mylist.FirstNode)
      
      //6. Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia);
      println("Lista Vazia? "+mylist.isEmpity())
      
      //7. Recuperar/Buscar um determinado elemento da lista;
      var node=mylist.find(3)
      
      //8. Remover um determinado elemento da lista;
      mylist.delete(3)
      
      //9. Remover um determinado elemento da lista usando recursão;
      mylist.deleteRecursive(4, mylist.FirstNode, mylist.FirstNode)
      
      //10. Liberar a lista;
      mylist.free()
      
      
      
      //1.Criar uma lista vazia;
      //2. Inserir elemento no início;
      //3. Imprimir os valores armazenados na lista;
      //4. Imprimir os valores armazenados na lista usando recursão;
      //5. Imprimir os valores armazenados na lista em ordem reversa (da cauda para a cabeça da lista);
      //6. Verificar se a lista está vazia (retorna 1 se vazia ou 0 se não vazia);
      //7. Recuperar/Buscar um determinado elemento da lista;
      //8. Remover um determinado elemento da lista;
      //9. Remover um determinado elemento da lista usando recursão;
      //10. Liberar a lista;
      
    }
  }