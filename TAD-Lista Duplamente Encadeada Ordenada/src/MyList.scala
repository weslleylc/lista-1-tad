

import scala.util.control._

class MyList {
  var FirstNode:Node = null 
  var LastNode:Node = null 
  
  var size=0;
  
  def createList:MyList = new  MyList()
  
  def insert(Value:Int){
    var newNode=new Node(Value)
    
    if (size == 0){
      FirstNode =  newNode
      LastNode = FirstNode
    }
    else{      
      var currNode=FirstNode
      var prevNode=FirstNode
      var condition = true
      
      while(condition){        
        if ((newNode.value<=currNode.value)||(currNode.NextNode==null))
            condition = false
        else {
          prevNode=currNode
          currNode=currNode.NextNode
          }        
      }
      
      if((newNode.value<currNode.value)&&(currNode==FirstNode)){
        currNode.setPrevNode(newNode)
        newNode.NextNode=currNode
        FirstNode=newNode 
      }      
      else
         if((currNode.NextNode==null)&&(newNode.value>currNode.value)){
          newNode.setPrevNode(LastNode)
          LastNode.setNode(newNode)
          LastNode=newNode
        }else{
          
          currNode.setPrevNode(newNode)
          newNode.NextNode=currNode
          
          prevNode.NextNode=newNode   
          newNode.setPrevNode(prevNode)
        }
      
    }
    size=size+1
  }
  
   def insertArray(A:Array[Int]){    
    var i=0
    for( i <- 0 to A.length-1){
        insert(A(i))
      }       
  }
  
  
  def isEmpity():Int={
    if (size==0) 1
    else 0
  }
   
  def printList(){
    var i =0;    
    var currNode=FirstNode
    for( i <- 1 to size){
      println( "Value of Node: " + currNode.value );
      currNode=currNode.NextNode
      }      
  }
  
  def printListRecursive(currNode: Node){
      println( "Value of Node: " + currNode.value );
      if (currNode.NextNode!=null)
        printListRecursive(currNode.NextNode) 
  }
  
  def printListReverse(){
      var i =0;    
      var currNode=LastNode
      for( i <- 1 to size){
      println( "Value of Node: " + currNode.value );
      currNode=currNode.PrevNode
      } 
  }
  
  def printListReverseLastNode(){
      var i =0;    
    var currNode=LastNode
    for( i <- 1 to size){
      println( "Value of Node: " + currNode.value );
      currNode=currNode.PrevNode
      }  

  }
  
  def find(value: Int):Node={
    var i =0;    
    var currNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){
        if(value==currNode.value){
          println( "Find Node "+value+" in position " + i );
          
          loop.break
        }else currNode=currNode.NextNode
      } 
      
      if(i!=size) println( "Not Find Node");
    }
    currNode
  }
  
  def delete(value: Int){
    
    var i =0;    
    var currNode=FirstNode    
    var prevNode=FirstNode
    val loop = new Breaks;
    loop.breakable {
      for( i <- 1 to size){        
        if(value==currNode.value){
          if(i==1){
            FirstNode=currNode.NextNode
            FirstNode.setPrevNode(null)
          }else if(i==size){
            LastNode=prevNode
            prevNode.NextNode=null;
          }else{
            prevNode.NextNode=currNode.NextNode 
            currNode.NextNode.setPrevNode(prevNode)
          }
          
          size=size-1
          println( "Deleted node "+ value);
          loop.break
        }else {
          prevNode=currNode
          currNode=currNode.NextNode
          }        
      } 
      
      if(i!=size) println( "Not Find Node:Can't delete");
    }
    
  }
  
  def deleteRecursive(value: Int,currNode: Node,prevNode: Node){
    if(currNode!=null){
      if(currNode.value==value){
        if(prevNode==currNode){          
            FirstNode=currNode.NextNode
            FirstNode.setPrevNode(null)
        }else if(currNode.NextNode==null){
            LastNode=prevNode
            prevNode.NextNode=null;
          }else{
            prevNode.NextNode=currNode.NextNode 
            currNode.NextNode.setPrevNode(prevNode)            
            }
        size=size-1
        println( "Deleted node (recursive)"+ value);
      }else deleteRecursive(value,currNode.NextNode,currNode)
    }  
  }
  
  def free(){
    recursiveFree(FirstNode)
    size=0;
    FirstNode=null;
    LastNode=null;
  }
  
  def recursiveFree(currNode: Node){  
      if (currNode!=null){
        recursiveFree(currNode.NextNode)
        currNode.NextNode=null;
      }             
  }
  
    def isEquals(list1: MyList, list2: MyList):Int={
    if (list1.size!=list2.size) 0
    else {  
      var currNode=list1.FirstNode
      var currNode2=list2.FirstNode
      
      var i = 0 
      
      for( i <- 1 to list1.size){
        if(currNode.value!=currNode2.value) 0
        else{
          currNode=currNode.NextNode
          currNode2=currNode2.NextNode
        }        
      }
      1
    }
  }
}